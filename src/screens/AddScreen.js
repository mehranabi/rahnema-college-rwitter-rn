import React, { Component } from 'react'
import { StyleSheet, ActivityIndicator } from 'react-native'
import {
  Container,
  Header,
  Content,
  Body,
  Title,
  Icon,
  Right,
  Left,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
} from 'native-base'
import { Formik } from 'formik'
import * as Yup from 'yup'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

const RwittValidationSchema = Yup.object().shape({
  user: Yup.string()
    .required('ضروری!')
    .min(3, 'حداقل 3 حرف وارد کنید!')
    .max(20, 'حداکثر 20 حرف میتوانید وارد کنید!'),
  message: Yup.string()
    .required('ضروری!')
    .min(5, 'حداقل 5 حرف وارد کنید!')
    .max(500, 'حداکثر 500 حرف میتوانید وارد کنید!'),
})

class Add extends Component {
  constructor(props) {
    super(props)
    this.state = {
      error: null,
      success: false,
    }
  }

  backPressed = () => {
    this.props.navigation.goBack()
  }

  onFormSubmitted = async (values, actions) => {
    actions.setSubmitting(true)
    await AsyncStorage.setItem('@user_name', values.user)
    Axios.post(
      'http://192.168.1.109:8080/messages',
      {
        ...values,
      },
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    )
      .then(res => {
        actions.setSubmitting(false)
        this.setState({
          success: true,
        })
      })
      .catch(err => {
        this.setState({
          error: err,
        })
      })
  }

  render() {
    return (
      <Container>
        <Header>
          <Right />
          <Body>
            <Title>Rwitt</Title>
          </Body>
          <Left>
            <Icon
              name="arrowleft"
              type="AntDesign"
              style={styles.backIcon}
              onPress={this.backPressed}
            />
          </Left>
        </Header>
        <Content>
          <Formik
            initialValues={{
              user: '',
              message: '',
            }}
            validationSchema={RwittValidationSchema}
            validateOnBlur
            onSubmit={this.onFormSubmitted}
            render={({
              isSubmitting,
              isValid,
              handleSubmit,
              setFieldValue,
              setFieldTouched,
              errors,
              touched,
            }) => (
              <Form>
                <Item
                  floatingLabel
                  error={touched.user && !!errors.user}
                  disabled={isSubmitting}
                >
                  <Label>نام</Label>
                  <Input
                    onBlur={() => setFieldTouched('user', true)}
                    onChangeText={txt => setFieldValue('user', txt)}
                  />
                </Item>
                {touched.user && !!errors.user && <Text>{errors.user}</Text>}
                <Item
                  floatingLabel
                  error={touched.message && !!errors.message}
                  disabled={isSubmitting}
                >
                  <Label>پیام</Label>
                  <Input
                    onBlur={() => setFieldTouched('message', true)}
                    onChangeText={txt => setFieldValue('message', txt)}
                  />
                </Item>
                {touched.message && !!errors.message && (
                  <Text>{errors.message}</Text>
                )}
                {isSubmitting ? (
                  <ActivityIndicator />
                ) : (
                  <Button
                    primary={this.state.error === null && !this.state.success}
                    success={this.state.success}
                    danger={this.state.error !== null}
                    full
                    disabled={!isValid}
                    onPress={handleSubmit}
                    style={styles.submitBtn}
                  >
                    <Text>
                      {this.state.error !== null
                        ? this.state.error.message
                        : this.state.success
                        ? 'ارسال شد'
                        : 'ارسال'}
                    </Text>
                  </Button>
                )}
              </Form>
            )}
          />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  backIcon: {
    color: 'white',
  },
  submitBtn: {
    marginTop: 10,
  },
})

export default Add
