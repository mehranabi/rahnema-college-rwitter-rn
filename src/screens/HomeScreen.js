import React, { Component } from 'react'
import { StyleSheet, FlatList, View } from 'react-native'
import {
  Container,
  Header,
  Content,
  Body,
  Title,
  Fab,
  Icon,
  Card,
  CardItem,
  Text,
  ListItem,
  Right,
  Left,
  Button,
} from 'native-base'
import moment from 'moment'
import Axios from 'axios'

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      messages: [],
      refreshing: true,
      error: null,
    }
  }

  goAddPage = () => {
    this.props.navigation.navigate('Add')
  }

  renderMessageItem = ({ item }) => (
    <ListItem>
      <Card style={styles.itemCard}>
        <CardItem header>
          <Text>{item.user}</Text>
        </CardItem>
        <CardItem>
          <Text>{item.message}</Text>
        </CardItem>
        <CardItem footer>
          <Text>{moment(item.timestamp).fromNow()}</Text>
        </CardItem>
      </Card>
    </ListItem>
  )

  extractMessageKey = item => item.id

  onRefresh = () => {
    this.setState(
      {
        refreshing: true,
      },
      this.fetchData
    )
  }

  componentDidMount() {
    this.fetchData()
  }

  fetchData = () => {
    Axios.get('http://192.168.1.109:8080/messages')
      .then(res => {
        this.setState({
          messages: res.data,
          refreshing: false,
        })
      })
      .catch(err => {
        this.setState({
          error: err,
        })
      })
  }

  onRerty = () => {
    this.setState(
      {
        error: null,
        refreshing: true,
      },
      this.fetchData
    )
  }

  render() {
    return (
      <Container>
        <Header>
          <Right />
          <Body>
            <Title>Rwitts</Title>
          </Body>
          <Left>
            <Icon
              name="retweet"
              type="AntDesign"
              style={styles.refreshIcon}
              onPress={this.onRefresh}
            />
          </Left>
        </Header>
        <Content>
          {this.state.error !== null ? (
            <View style={styles.errorContainer}>
              <Text style={styles.errorText}>{this.state.error.message}</Text>
              <Button onPress={this.onRerty}>
                <Text>Retry</Text>
              </Button>
            </View>
          ) : (
            <FlatList
              data={this.state.messages}
              renderItem={this.renderMessageItem}
              keyExtractor={this.extractMessageKey}
              onRefresh={this.onRefresh}
              refreshing={this.state.refreshing}
            />
          )}
        </Content>
        <Fab position="bottomRight" style={styles.fab} onPress={this.goAddPage}>
          <Icon name="add" />
        </Fab>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  fab: {
    backgroundColor: '#5067FF',
  },
  itemCard: {
    flex: 1,
  },
  errorText: {
    color: 'red',
  },
  errorContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  refreshIcon: {
    color: 'white',
  },
})

export default Home
