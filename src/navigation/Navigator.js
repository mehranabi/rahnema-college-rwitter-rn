import React from 'react'
import { createStackNavigator, createAppContainer } from 'react-navigation'

import HomeScreen from '../screens/HomeScreen'
import AddScreen from '../screens/AddScreen'

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Add: {
      screen: AddScreen,
    },
  },
  {
    defaultNavigationOptions: {
      header: null,
    },
  }
)

export default createAppContainer(AppNavigator)
